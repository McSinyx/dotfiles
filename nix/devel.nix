{ lib, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    bat fd jq ripgrep vim_configurable vimpager-latest
    exa gitAndTools.gitFull minicom man-pages man-pages-posix rlwrap
    gcc go guile_3_0 lua rakudo zig
    bintools gdb gnumake pkg-config
    texlive.combined.scheme-full
    (python3.withPackages (pypkgs: with pypkgs; [ flit rsskey ]))
  ];

  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
    pinentryFlavor = "qt";
  };

  services.openssh = {
    enable = true;
    settings = {
      passwordAuthentication = false;
      kbdInteractiveAuthentication = false;
    };
  };

  virtualisation.podman = {
    dockerCompat = true;
    enable = true;
  };
}

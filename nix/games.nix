{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    _20kly hedgewars scorched3d zeroad # strategy
    astromenace chromium-bsu # scrolling
    taisei # shmup
    bastet moon-buggy n2048 ttyper # text
    blackshades xonotic-glx # fps
    blobwars superTux teeworlds # platformer
    bonzomatic # engine
    extremetuxracer gl117 gltron superTuxKart # vehicle
    flare freedroidrpg hyperrogue # slash
    frozen-bubble pinball xbill # coffeebreak
    neverball # puzzle
  ];
  hardware.opengl.driSupport32Bit = true;
}

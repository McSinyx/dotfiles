# Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running 'nixos-help').

{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./awesome.nix
    ./devel.nix
    ./games.nix
  ];

  boot = {
    supportedFilesystems = [ "btrfs" ];
    loader = {
      efi.canTouchEfiVariables = true;
      grub = {
        enable = true;
        device = "nodev";
        efiSupport = true;
        useOSProber = true;
        splashImage = null;
      };
    };
  };

  networking = {
    hostName = "nix";
    hosts = let
      loang = [
        "danh.loang.net" "tin.loang.net" "than.loang.net" "xem.loang.net"
        "cnx.gdn" "px.cnx.gdn" "src.cnx.gdn"
      ];
    in {
      "127.0.0.1" = [ "9gag.com" "voz.vn" ];
      "37.205.11.127" = loang;
      "2a03:3b40:100::1:2" = loang;
    };
    nameservers = [
      # ns5.ca.us.dns.opennic.glue
      "137.184.12.79" "2604:a880:4:1d0::4d:e000"
      # ns4.au.dns.opennic.glue
      "168.138.8.38" "2603:c023:c002:f4aa:a208:5df4:ee55:b70a"
    ];
    networkmanager = {
      enable = true;
      insertNameservers = config.networking.nameservers;
    };

    nftables = {
      enable = true;
      ruleset = ''
        table inet filter {
          # https://www.cloudflare.com/ips-v4
          set cloudflare_ipv4 {
            type ipv4_addr
            flags interval
            elements = {
              173.245.48.0/20,
              103.21.244.0/22,
              103.22.200.0/22,
              103.31.4.0/22,
              141.101.64.0/18,
              108.162.192.0/18,
              190.93.240.0/20,
              188.114.96.0/20,
              197.234.240.0/22,
              198.41.128.0/17,
              162.158.0.0/15,
              104.16.0.0/13,
              104.24.0.0/14,
              172.64.0.0/13,
              131.0.72.0/22
            }
          }

          # https://www.cloudflare.com/ips-v6
          set cloudflare_ipv6 {
            type ipv6_addr
            flags interval
            elements = {
              2400:cb00::/32,
              2606:4700::/32,
              2803:f800::/32,
              2405:b500::/32,
              2405:8100::/32,
              2a06:98c0::/29,
              2c0f:f248::/32
            }
          }

          chain output {
            type filter hook output priority 0
            policy accept

            ip daddr @cloudflare_ipv4 counter reject
            ip6 daddr @cloudflare_ipv6 counter reject
          }

          chain input {
            type filter hook output priority 0
            policy accept

            ip saddr @cloudflare_ipv4 counter reject
            ip6 saddr @cloudflare_ipv6 counter reject
          }
        }
      '';
    };

    interfaces = {
      enp1s0.useDHCP = true;
      wlp2s0.useDHCP = true;
    };

    # Configure network proxy if necessary
    # proxy.default = "http://user:password@proxy:port/";
    # proxy.noProxy = "127.0.0.1,localhost,internal.domain";

    # Open ports in the firewall.
    firewall.allowedTCPPorts = [ 59879 ];
    firewall.allowedUDPPorts = [ 4001 ];
  };

  # Set your time zone.
  time.timeZone = "Asia/Seoul";

  # Select internationalisation properties.
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  users.users.sea = {
    isNormalUser = true;
    extraGroups = [ "dialout" "ipfs" "networkmanager" "video" "wheel" ];
  };

  environment = {
    localBinInPath = true;
    systemPackages = with pkgs; [
      atool compsize cryptsetup htop-vim inotify-tools killall rsync unzip zip
      oathToolkit isync stow tor w3m wget yt-dlp
      imv mpv pavucontrol sent yacreader
      ffmpeg mkvtoolnix mediainfo simplescreenrecorder sox
      # darktable
      gimp imagemagick pdftk
    ];
    wordlist.enable = true;
  };

  programs.iotop.enable = true;
  programs.udevil.enable = true;

  security.rtkit.enable = true;
  services = {
    dictd = {
      enable = true;
      DBs = with pkgs.dictdDBs; [ wiktionary wordnet ];
    };

    pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
    };

    udisks2.enable = true;
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}

{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; let wee-slack = weechat.override {
    configure = { ... }: {
      scripts = [ weechatScripts.wee-slack ];
    };
  };
  in [
    clipbuzz keynav labwc playerctl pulsemixer xclip xdotool xorg.xkill
    aerc amfora calcurse dante khard mu ncdu ranger senpai ueberzug
    arandr audacious foot liferea scrot rxvt-unicode zathura
    firefox libreoffice nheko tor-browser-bundle-bin wee-slack
    networkmanagerapplet nextcloud-client transmission-gtk
    gnome.adwaita-icon-theme qt5ct
  ];

  fonts.fonts = with pkgs; [ lmodern noto-fonts-cjk-sans noto-fonts-emoji ];

  hardware.brillo.enable = true;

  i18n = {
    defaultLocale = "en_US.UTF-8";
    inputMethod = {
      enabled = "ibus";
      ibus.engines = with pkgs.ibus-engines; [
        hangul
        table table-others
      ];
    };
  };

  programs = {
    mepo.enable = true;
    nm-applet.enable = true;
    slock.enable = true;
  };

  qt = {
    enable = true;
    platformTheme = "qt5ct";
  };

  services = {
    autorandr.enable = true;
    xserver = {
      videoDrivers = [ "intel" ];
      deviceSection = ''
        Option "TearFree" "true"
      '';

      enable = true;
      desktopManager.xterm.enable = false;
      displayManager.startx.enable = true;
      windowManager.awesome.enable = true;

      layout = "us";
      xkbOptions = "caps:ctrl_modifier,compose:menu";
      libinput.enable = true; # Enable touchpad support.
    };
  };
}

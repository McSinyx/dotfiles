# dotfiles

Dotfiles of my Debian and NixOS systems, managed by GNU Stow.

![Screenshot](screenshot.png)

Both uses awesome, bash, Git, Firefox, ranger, Vim, URxvt
and Zathura. Most of these applications are themed with the
[srcery](https://github.com/srcery-colors/srcery-vim) colorscheme.

(use-modules (gnu home)
             (gnu home services)
             (gnu home services desktop)
             (gnu home services mcron)
             (gnu home services shells)
             (gnu packages)
             (gnu services)
             (guix gexp))

(home-environment
  (packages
    (specifications->packages
      '("aerc" "isync" "mu" "ncurses" "nheko" "weechat" "weechat-wee-slack"
        "arandr" "libreoffice" "rxvt-unicode"
        "audacious" "ffmpeg" "mediainfo" "mpv" "simplescreenrecorder" "yt-dlp"
        "awesome" "copyq" "dbus" "keynav" "scrot" "sx" "xdg-utils" "xrdb"
        "bastet" "hedgewars"
        "font-google-noto" "font-latin-modern" "qt5ct"
        "gcc" "python" "texlive"
        "gimp" "imv" "imagemagick" "zathura" "zathura-pdf-poppler"
        "git" "git:send-email" "git-lfs" "nss-certs" "sshfs" "stow" "unzip"
        "gnupg" "pinentry" "oath-toolkit"
        "ibus" "ibus-libhangul"
        "icecat" "liferea" "transmission:gui" "w3m"
        "pavucontrol" "playerctl" "pulsemixer")))
  (services
    (list (service home-bash-service-type
                   (home-bash-configuration
                     (bash-profile (list (local-file "./bash-profile")))
                     (bashrc (list (local-file "./bashrc")))))
          (simple-service 'more-home-environment-variables-service
                          home-environment-variables-service-type
                          `(("SHELL" . #t)
                            ("PATH" . "$HOME/.local/bin:$PATH")
                            ("GOPATH" . "$HOME/.local/share/go")
                            ("NIX_PATH" . "$HOME/Sauces")
                            ("EDITOR" . "vim")
                            ("GPG_TTY" . "$(tty)")
                            ("QT_QPA_PLATFORMTHEME" . "qt5ct")))
          (service home-dbus-service-type)
          (service home-mcron-service-type
                   (home-mcron-configuration
                     (jobs (list #~(job '(next-minute (range 0 60 1))
                                        (lambda ()
                                          (system* "mbsync" "--all"))))))))))

;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu))
(use-service-modules avahi desktop dns networking nix sound ssh xorg)

(operating-system
  (locale "en_DK.utf8")
  (timezone "Asia/Seoul")
  (keyboard-layout (keyboard-layout "us" #:options '("ctrl:nocaps"
                                                     "compose:menu")))
  (host-name "guix")
  (users (cons* (user-account
                  (name "cnx")
                  (comment "McSinyx")
                  (group "users")
                  (home-directory "/home/cnx")
                  (supplementary-groups '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append
      (specifications->packages
        '("bash-completion" "curl" "fd" "file" "htop"
          "nss-certs" "ranger" "ripgrep" "rsync" "vim-full"))
      %base-packages))
  (services
    (cons* (service openssh-service-type
                    (openssh-configuration
                      (password-authentication? #f)))
           (service xorg-server-service-type)
           (set-xorg-configuration
             (xorg-configuration
               (keyboard-layout keyboard-layout)))
           (screen-locker-service (specification->package "slock"))
           polkit-wheel-service
           (service nftables-service-type
                    (nftables-configuration
                      (ruleset (local-file "./nftables.conf"))))
           (service static-networking-service-type
                    (list (static-networking
                            (addresses
                              (list (network-address
                                      (device "enp5s0")
                                      (value "192.168.0.7/24"))))
                            (routes
                              (list (network-route
                                      (destination "default")
                                      (gateway "192.168.0.1"))))
                            (name-servers ; ns{4,5}.ca.us.dns.opennic.glue
                              '("147.182.243.49" "137.184.12.79")))))
           (service avahi-service-type)
           (udisks-service)
           (elogind-service)
           (service ntp-service-type)
           x11-socket-directory-service
           (service pulseaudio-service-type)
           (service alsa-service-type)
           (service nix-service-type)
           %base-services))
  (kernel-arguments (cons "modprobe.blacklist=pcspkr"
                          %default-kernel-arguments))
  (bootloader
    (bootloader-configuration
      (bootloader grub-efi-bootloader)
      (targets (list "/boot/efi"))
      (keyboard-layout keyboard-layout)))
  (swap-devices
    (list (swap-space (target (uuid "2e1d6b10-ef93-47f0-8eb0-b0ebf406e99b")))))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device (uuid "f2cc88e7-55d3-4fce-9bc0-68a38c9671f5" 'btrfs))
             (type "btrfs"))
           (file-system
             (mount-point "/boot/efi")
             (device (uuid "09F6-ABE3" 'fat32))
             (type "vfat"))
           %base-file-systems)))

set nocompatible
set undodir=~/.cache/vim/undo
set directory=~/.cache/vim/swap
set backupdir=~/.cache/vim/backup
set viminfo+=n~/.cache/vim/viminfo
set clipboard=unnamedplus autochdir
set showcmd noshowmode ruler wildmenu confirm number relativenumber
set nostartofline
if has("patch-7.4.710")
  set list listchars+=space:·,tab:\ \ 
endif
set tabstop=8 expandtab shiftwidth=4 softtabstop=-1 smarttab
set ignorecase infercase dictionary=~/.vim/words.txt
set keymap=vietnamese-telex imdisable iminsert=0 imsearch=-1
set omnifunc=syntaxcomplete#Complete
set diffopt+=algorithm:patience

augroup vimrc
  autocmd!
  autocmd BufNewFile,BufRead *.vert,*.geom,*.frag setlocal filetype=glsl
  autocmd BufNewFile,BufRead *.info setlocal filetype=json
  autocmd BufNewFile,BufRead *.tsv setlocal filetype=tsv
  autocmd BufNewFile,BufRead *.PAS setlocal filetype=pascal
  autocmd BufNewFile,BufRead *.ms setlocal filetype=groff
  autocmd BufNewFile,BufRead *.m setlocal filetype=octave
  autocmd BufNewFile,BufRead *.h setlocal filetype=c
  autocmd BufNewFile,BufRead CHANGES setlocal filetype=mail
  autocmd FileType asm,c,cpp,h,diff,gitconfig,gitsendemail,go,glsl,mail,make,sshconfig,php,tsv
        \ setlocal cindent cinoptions=(0 noexpandtab shiftwidth=8 tabstop=8
  autocmd FileType vim,sh,scheme,lua,tex,cmake,cpp,plantuml,html,octave,pascal
        \ setlocal shiftwidth=2
  autocmd FileType rst setlocal shiftwidth=3
  autocmd FileType mail,markdown,rst,tex setlocal spell
  autocmd BufWinEnter *
        \ if &filetype ==# 'python' || &filetype ==# 'cython'
        \ || &filetype ==# 'mail'
        \ | let w:m1=matchadd('ColorColumn', '\%<80v.\%>73v', -1) |
        \ else
        \ | let w:m1=matchadd('ColorColumn', '\%<81v.\%>80v', -1) |
        \ endif
augroup END

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:srcery_italic = 1
let g:jedi#popup_on_dot = 0
let g:jedi#popup_select_first = 0
let g:jedi#show_call_signatures = 2
let g:jedi#smart_auto_mappings = 0
let g:polyglot_disabled = ['latex']

call plug#begin('~/.vim/plugged')
Plug 'https://github.com/vim/killersheep'
Plug 'https://github.com/sheerun/vim-polyglot'
Plug 'https://github.com/francoiscabrol/ranger.vim'
Plug 'https://github.com/srcery-colors/srcery-vim'
Plug 'https://github.com/tpope/vim-unimpaired'
Plug 'https://github.com/davidhalter/jedi-vim', {'for': 'python'}
Plug 'https://github.com/lervag/vimtex', {'for': 'tex'}
Plug 'https://github.com/kovisoft/slimv', {'for': 'lisp'}
Plug 'https://github.com/tpope/vim-fireplace', {'for': 'clojure'}
Plug 'https://github.com/anntzer/vim-cython', {'for': 'cython'}
Plug 'https://git.sr.ht/~sircmpwn/hare.vim', {'for': 'hare'}
call plug#end()

set t_Co=256
colorscheme srcery
map Q gq
command Q q
command W w
nmap W :w<CR>
imap <C-x><C-x> <C-^>
